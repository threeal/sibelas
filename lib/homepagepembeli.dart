import 'package:flutter/material.dart';

import './mainpagepembeli_home.dart' as home;
import './mainpagepembeli_search.dart' as search;
import './mainpagepembeli_akun.dart' as akun;
import './mainpagepembeli_transaction.dart' as transact;

void main() {
  runApp(new MaterialApp(
    title: 'Lappy',
    home: HomePagePembeli(),
  ));
}

class HomePagePembeli extends StatefulWidget {
  @override
  _HomePagePembeliState createState() => _HomePagePembeliState();
}

class _HomePagePembeliState extends State<HomePagePembeli>
    with SingleTickerProviderStateMixin {

  TabController controller;
@override
  void initState() {
    controller = new TabController(vsync: this, length: 4);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SiBELAS'),
        backgroundColor: Colors.blueAccent[700],
        automaticallyImplyLeading: false,
      ),
      body: TabBarView(
        controller: controller,
        children: <Widget>[
          home.Home(),
          search.Search(),
          transact.Transaction(),
          akun.Account(),

        ],
      ),
      bottomNavigationBar: Material(
        color: Colors.lightBlueAccent[700],
        child: TabBar(
        controller: controller,
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.home),
            ),
            Tab(
              icon: Icon(Icons.search),
            ),
            Tab(
              icon: Icon(Icons.compare_arrows),
            ),
            Tab(
              icon: Icon(Icons.account_box),
            ),
          ],
      ),
      ),
    );
  }
}
