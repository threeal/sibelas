import 'package:flutter/material.dart';
import 'package:lappy/signuppage.dart';
import './homepagepembeli.dart';

void main() {
  runApp(MaterialApp(
    home: LoginPage(),
  ));
}

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        backgroundColor: Colors.lightBlueAccent[700],
      ),
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [
              0.1,
              1.0
            ],
                colors: [
              Colors.lightBlueAccent[700],
              Colors.lightBlue[50],
            ])),
        child: Center(
          child: Container(
            width: 300.0,
            height: 600.0,
            //color: Colors.green,
            child: Center(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 25.0),
                    child: Center(
                        child: Text(
                      'Masukkan Email dan Password Akunmu',
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                      textAlign: TextAlign.center,
                    )),
                  ),
                  TextForm(
                    label: 'Email',
                    obscure: false,
                    maxline: 1,
                  ),
                  TextForm(
                    label: 'Password',
                    obscure: true,
                    maxline: 1,
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      MaterialButton(
                        color: Colors.blueGrey,
                        onPressed: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      HomePagePembeli()));
                        },
                        child: Text(
                          'Login Pembeli',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        width: 30.0,

                      ),
                      MaterialButton(
                        color: Colors.blueGrey,
                        onPressed: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      HomePagePembeli()));
                        },
                        child: Text(
                          'Login Penjual',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Belum punya akun?'),
                      MaterialButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SignUpPage()));
                        },
                        child: Text(
                          'daftar disini',
                          style: TextStyle(color: Colors.blue[900]),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class TextForm extends StatelessWidget {
  TextForm({this.label, this.obscure, this.maxline});
  final String label;
  final bool obscure;
  final int maxline;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: TextField(
        obscureText: obscure,
        decoration: InputDecoration(
          hintText: label,
          border: OutlineInputBorder(),
        ),
        maxLines: maxline,
      ),
    );
  }
}
