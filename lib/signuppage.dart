import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: SignUpPage(),
  ));
}

class SignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Daftar Akun'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 40.0),
        child: ListView(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    'Masukan Data Dirimu',
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                TextForm(
                  label: 'Nama Lengkap',
                  obscure: false,
                  maxline: 1,
                ),
                TextForm(
                  label: 'Email',
                  obscure: false,
                  maxline: 1,
                ),
                TextForm(
                  label: 'Alamat',
                  obscure: false,
                  maxline: 5,
                ),
                TextForm(
                  label: 'No. Handphone',
                  obscure: false,
                  maxline: 1,
                ),
                TextForm(
                  label: 'Password',
                  obscure: true,
                  maxline: 1,
                ),
                MaterialButton(
                  minWidth: 50.0,
                  color: Colors.blueGrey,
                  child: Text('Daftar',style: TextStyle(fontSize: 20.0),),
                  onPressed: () {
                  },

                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class TextForm extends StatelessWidget {
  TextForm({this.label, this.obscure, this.maxline});
  final String label;
  final bool obscure;
  final int maxline;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: TextField(
        obscureText: obscure,
        decoration: InputDecoration(
          hintText: label,
          border: OutlineInputBorder(),
        ),
        maxLines: maxline,
      ),
    );
  }
}
