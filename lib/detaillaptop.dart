import 'package:flutter/material.dart';

class Detail extends StatefulWidget {
  final List list;
  final int index;
  Detail({this.list, this.index});
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.list[widget.index]['nama']}"),
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: ListView(
            children: <Widget>[
              Image.network("${widget.list[widget.index]['foto']}"),
              SizedBox(height: 20.0,),
              Text(
                widget.list[widget.index]['nama'],
                style: TextStyle(fontSize: 20.0),
                textAlign: TextAlign.center,
              ),
              Text(
                "Harga : ${widget.list[widget.index]['harga']}",
                style: TextStyle(fontSize: 20.0),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
