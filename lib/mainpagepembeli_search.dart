import 'package:flutter/material.dart';
import 'package:lappy/loginpage.dart';

void main() {
  runApp(MaterialApp(
    home: Search(),
  ));
}

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  List<String> jenislaptop = ["Standart", "Bisnis", "Desain", "Gaming"];
  String jlaptop = "Standart";

  List<String> datasatu = ["13 Inch", "14 Inch", "15 Inch"];
  String variabelsatu = "13 Inch";
  String label = 'Ukuran layar : ';

  List<String> datadua = ["Polycarbonate", "Alumunium Alloy"];
  String variabeldua = "Polycarbonate";
  String labeldua = 'Bahan body : ';

  void pilihlaptop(String value) {
    setState(() {
      jlaptop = value;
    });
  }

  void pilihsatu(String value) {
    setState(() {
      variabelsatu = value;
    });
  }

  void pilihdua(String value) {
    setState(() {
      variabeldua = value;
    });
  }

  void kondisi() {
    if (jlaptop == 'Gaming') {
      label = 'Genre game : ';
      datasatu = ["FPS", "RTS","RPG"];
      variabelsatu = "FPS";
      labeldua = 'Refresh rate layar : ';
      datadua = ["90Hz","120Hz","144Hz","240Hz"];
      variabeldua = "90Hz";
    } 
    else if(jlaptop =='Bisnis'){
      label = 'Fitur keamanan : ';
      datasatu = ["Ya", "Tidak"];
      variabelsatu = "Ya";
      labeldua = 'Support touchscreen : ';
      datadua = ["Ya","Tidak"];
      variabeldua = "Ya";
    }
    else if (jlaptop == 'Desain'){
      label = 'Support stylus : ';
      datasatu = ["Ya", "Tidak"];
      variabelsatu = "Ya";
      labeldua = 'Jenis desain : ';
      datadua = ["Gambar 2D", "Gambar 3D", "Video"];
      variabeldua = "Gambar 2D";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 20.0),
          child: ListView(
            children: <Widget>[
              TextForm(
                label: 'cari',
                obscure: false,
              ),
                MaterialButton(
                color: Colors.blueAccent,
                onPressed: (){},
                child: Text("Cari", style: TextStyle(color: Colors.white, fontSize: 15.0),),
              ),
              Padding(padding:EdgeInsets.symmetric(vertical: 20.0),child: Center(child: Text('Atau'))),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text('Kebutuhan utama : ',
                      style:
                          TextStyle(fontSize: 20.0, color: Colors.blueAccent)),
                  DropdownButton(
                    onChanged: (String value) {
                      pilihlaptop(value);
                      kondisi();
                    },
                    value: jlaptop,
                    items: jenislaptop.map((String value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(label,
                      style:
                          TextStyle(fontSize: 20.0, color: Colors.blueAccent)),
                  DropdownButton(
                    onChanged: (String value) {
                      pilihsatu(value);
                    },
                    value: variabelsatu,
                    items: datasatu.map((String value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(labeldua,
                      style:
                          TextStyle(fontSize: 20.0, color: Colors.blueAccent)),
                  DropdownButton(
                    onChanged: (String value) {
                      pilihdua(value);
                    },
                    value: variabeldua,
                    items: datadua.map((String value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.symmetric(vertical: 20.0),),
              MaterialButton(
                color: Colors.blueAccent,
                onPressed: (){},
                child: Text("Cari", style: TextStyle(color: Colors.white, fontSize: 15.0),),
              )
            ],
          ),
        ),
      ),
    ));
  }
}
