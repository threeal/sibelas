import 'package:flutter/material.dart';
import './loginpage.dart';

void main() {
  runApp(new MaterialApp(
    home: new FirstPage(),
  ));
}

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Stack(
      children: <Widget>[
        new Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [
                0.1,
                1.0
              ],
                  colors: [
                Colors.lightBlueAccent[700],
                Colors.lightBlue[50],
              ])),
        ),
        Center(
          child: Container(
            width: 300.0,
            height: 300.0,
            //color: Colors.yellow,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'images/download2.png',
                  width: 100.0,
                ),
                Text(
                  "SiBELAS",
                  style: TextStyle(
                      fontSize: 60.0,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic),
                ),
                Text(
                  "Aplikasi Jual-Beli Laptop Cerdas",
                  style: TextStyle(fontSize: 15.0, fontStyle: FontStyle.italic),
                ),
                SizedBox(
                  height: 50.0,
                ),
                MaterialButton(
                  minWidth: 100.0,
                  height: 40.0,
                  color: Colors.blueGrey[600],
                  onPressed: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
                  },
                  child: Text(
                    "Next",
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    ));
  }
}


class TextForm extends StatelessWidget {
  TextForm({this.label,this.obscure, this.maxline});
  final String label;
  final bool obscure;
  final int maxline;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: TextField(
          obscureText: obscure,
          decoration: InputDecoration(
            hintText: label,
            border: OutlineInputBorder(),
          ),
          maxLines: maxline,
          ),
    );
  }
}
